# Flectra Community / web-api

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[endpoint_auth_api_key](endpoint_auth_api_key/) | 2.0.1.2.1| Provide API key auth for endpoints.
[endpoint](endpoint/) | 2.0.2.4.0| Provide custom endpoint machinery.
[endpoint_cache](endpoint_cache/) | 2.0.1.1.2| Provide basic caching utils for endpoints
[endpoint_route_handler](endpoint_route_handler/) | 2.0.2.1.1| Provide mixin and tool to generate custom endpoints on the fly.
[endpoint_jsonifier](endpoint_jsonifier/) | 2.0.1.1.0| Allow to configure jsonifier parsers on endpoints
[webservice](webservice/) | 2.0.1.5.0|     Provide unified way to handle external webservices configuration and calls.    


